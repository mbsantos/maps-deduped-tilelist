import argparse
import sys

from tileset import TileSet


def main():
    parser = argparse.ArgumentParser(
        description="Given a tile list as input, generate a distinct "
        "list of map tiles consisted of all the parent and children tiles "
        "recursively up to a zoom level."
    )
    parser.add_argument(
        "minzoom",
        default=0,
        help="The minimum zoom level of the generated tilelist.",
        type=int,
    )
    parser.add_argument(
        "maxzoom", help="The maximum zoom level of the generated tilelist.", type=int
    )
    parser.add_argument(
        "tilelist_file",
        help="Path to the tilelist input file.",
        type=argparse.FileType("r"),
        default=sys.stdin,
        nargs="?",
    )
    args = parser.parse_args()

    ts = TileSet(args.tilelist_file, args.minzoom, args.maxzoom)
    ts.read()

    for tile in ts.tileset:
        print(f"{tile.z}/{tile.x}/{tile.y}")
