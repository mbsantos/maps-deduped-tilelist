from argparse import Namespace
from io import StringIO
from unittest import TestCase, mock

from tileset.cli import main


class CliTest(TestCase):
    @mock.patch(
        "argparse.ArgumentParser.parse_args",
        return_value=Namespace(tilelist_path="/path/to/tilelist", minzoom=0, maxzoom=1),
    )
    def test_main(self, mock_parse_args):
        test_input = "\n".join(["0/0/0", "1/0/0"])
        test_input_file = StringIO()
        test_input_file.write(test_input)
        test_input_file.seek(0)

        with mock.patch("tileset.cli.argparse.ArgumentParser.parse_args") as parse_args:
            parse_args.return_value = Namespace(
                tilelist_file=test_input_file, minzoom=0, maxzoom=1
            )
            with mock.patch("builtins.print") as mock_print:
                main()
                call_list = [
                    mock.call("1/0/1"),
                    mock.call("1/1/0"),
                    mock.call("0/0/0"),
                    mock.call("1/0/0"),
                    mock.call("1/1/1"),
                ]
                mock_print.assert_has_calls(call_list, any_order=True)
