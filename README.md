# Deduped map tilelist

Given an input file with a list of line seperated tiles in `{zoom}/{x}/{y}` format, generate a distinct list of map tiles for all zoom levels from `minzoom` up to `maxzoom`.

## Usage

```
usage: maps-deduped-tilelist [-h] minzoom maxzoom [tilelist_file]

Given a tile list as input, generate a distinct list of map tiles consisted of all the parent and children tiles recursively up to a zoom level.

positional arguments:
  minzoom        The minimum zoom level of the generated tilelist.
  maxzoom        The maximum zoom level of the generated tilelist.
  tilelist_file  Path to the tilelist input file.

optional arguments:
  -h, --help     show this help message and exit
```

## Example

```
> cat tilelist.txt
0/0/0
0/1/0
> cat tilelist.txt | maps-deduped-tilelist 0 1
1/2/1
1/0/1
1/1/0
0/1/0
1/2/0
0/0/0
1/3/1
1/0/0
1/3/0
1/1/1
> maps-deduped-tilelist 0 1 ./tilelist.txt
1/2/1
1/0/1
1/1/0
0/1/0
1/2/0
0/0/0
1/3/1
1/0/0
1/3/0
1/1/1
```

## Context

This CLI tool is built as a way to generate a deduplicated tile list of distinct tiles based on the `imposm3` expired tile output. The reason behind this is to deduplicate tiles and optimize tile pregeneration by avoiding generating the same tile multiple times.

## Dependencies

- Python >= 3.7

## Releases

Project releases are created automatically when a new tag is created. To create a new release with proper packaging:

- Bump the version under `setup.py`
- Bump the version under `debian/changelog`
- Push a new tag with the changes
